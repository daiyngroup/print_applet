package printapplet;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.List;
import java.awt.Point;
import java.awt.Toolkit;
import javax.swing.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import static javax.swing.SwingConstants.CENTER;
import org.apache.commons.io.FileUtils;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.entity.StringEntity;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.printing.PDFPageable;
import org.json.JSONArray;
import org.json.JSONObject;
 
 
class PrintOrder implements ActionListener {
    JFrame f;
    JTextField t;
    JButton b1,b2,b3,b4,b5,b6,b7,b8,b9,b0,bdiv,bmul,bsub,badd,bdec,beq,bdel,bclr, bprint;        
 
    static double a=0,b=0,result=0;
    static int operator=0;
 
    
    String path1 = "C:/Documents and Settings/Admin/Рабочий стол/SendStatus/dist/";
    //String path1 = "C:/Users/Personal/Documents/NetBeansProjects/SendStatus/dist/";
        
         int pagesNumber;
        
        
        
        int list_count = 0;
        
    public static int height = Toolkit.getDefaultToolkit().getScreenSize().height;
    public static int width = Toolkit.getDefaultToolkit().getScreenSize().width;
    
    public PrintOrder() {
        f=new JFrame("Calculator");
        t=new JTextField();
        b1=new JButton("1");
        b2=new JButton("2");
        b3=new JButton("3");
        b4=new JButton("4");
        b5=new JButton("5");
        b6=new JButton("6");
        b7=new JButton("7");
        b8=new JButton("8");
        b9=new JButton("9");
        b0=new JButton("0");        
        bdel=new JButton("<");
        bprint = new JButton("На печать");
        
        BufferedImage cursorImg = new BufferedImage(16, 16, BufferedImage.TYPE_INT_ARGB);

// Create a new blank cursor.
Cursor blankCursor = Toolkit.getDefaultToolkit().createCustomCursor(
    cursorImg, new Point(0, 0), "blank cursor");

// Set the blank cursor to the JFrame.
f.getContentPane().setCursor(blankCursor);
        
        Font font = t.getFont().deriveFont((float)60);
        Font font2 = t.getFont().deriveFont((float)40);
        
        t.setBounds(width/2-250,200,470,100);        
        t.setFont(font);
        t.setEditable(false);
        t.setHorizontalAlignment(JTextField.CENTER);
        t.setBackground(Color.WHITE);
        b7.setBounds(width/2-250,620,150,100);
        b7.setFont(font);
        b8.setBounds(width/2-90,620,150,100);
        b8.setFont(font);
        b9.setBounds(width/2+70,620,150,100);        
        b9.setFont(font);
        
        b4.setBounds(width/2-250,480,150,100);
        b4.setFont(font);
        b5.setBounds(width/2-90, 480,150,100);
        b5.setFont(font);
        b6.setBounds(width/2+70, 480,150,100);        
        b6.setFont(font);
        
        b1.setBounds(width/2-250,340,150,100);
        b1.setFont(font);
        b2.setBounds(width/2-90,340,150,100);
        b2.setFont(font);
        b3.setBounds(width/2+70,340,150,100); 
        b3.setFont(font);
        
        b0.setBounds(width/2-90,760,150,100);
        b0.setFont(font);        
        
        bdel.setBounds(width/2+260,200,250,100);
        bdel.setFont(font);
        
        bprint.setBounds(width/2+260, 340, 250, 100);
        bprint.setFont(font2);
        
        f.add(t);
        f.add(b7);
        f.add(b8);
        f.add(b9);        
        f.add(b4);
        f.add(b5);
        f.add(b6);        
        f.add(b1);
        f.add(b2);
        f.add(b3);        
        f.add(b0);        
        f.add(bdel);        
        f.add(bprint);
        f.setUndecorated(true);
        f.setLayout(null);       
        f.setVisible(true);
        f.setSize(350,500);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setResizable(false);        
        Dimension sSize = Toolkit.getDefaultToolkit().getScreenSize();
        f.setSize(sSize);
        
        b1.addActionListener(this);
        b2.addActionListener(this);
        b3.addActionListener(this);
        b4.addActionListener(this);
        b5.addActionListener(this);
        b6.addActionListener(this);
        b7.addActionListener(this);
        b8.addActionListener(this);
        b9.addActionListener(this);
        b0.addActionListener(this);        
        bdel.addActionListener(this);      
        bprint.addActionListener(this);
    }
 
    public void actionPerformed(ActionEvent e)
    {
        if(e.getSource()==b1)
            t.setText(t.getText().concat("1"));
        
        if(e.getSource()==b2)
            t.setText(t.getText().concat("2"));
        
        if(e.getSource()==b3)
            t.setText(t.getText().concat("3"));
        
        if(e.getSource()==b4)
            t.setText(t.getText().concat("4"));
        
        if(e.getSource()==b5)
            t.setText(t.getText().concat("5"));
        
        if(e.getSource()==b6)
            t.setText(t.getText().concat("6"));
        
        if(e.getSource()==b7)
            t.setText(t.getText().concat("7"));
        
        if(e.getSource()==b8)
            t.setText(t.getText().concat("8"));
        
        if(e.getSource()==b9)
            t.setText(t.getText().concat("9"));
        
        if(e.getSource()==b0)
            t.setText(t.getText().concat("0"));               
                
        if(e.getSource()==bdel)
        {            
            String s=t.getText();
            t.setText("");
            for(int i=0;i<s.length()-1;i++)
            t.setText(t.getText()+s.charAt(i));
        }        
        
        if (e.getSource() == bprint) {
            try {
                if (checkInternetConnection()) {
                    sendOrderNum();
                } else {
                    showDialogErrorMessage("Ошибка подключения к интернету");
                }
                //testLink();
            } catch (InterruptedException ex) {
                Logger.getLogger(PrintOrder.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(PrintOrder.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
    }
 
    public static void main(String...s)
    {
        new PrintOrder();
    }

    private void sendOrderNum() {
        HttpClient httpClient = new DefaultHttpClient();        

    try {       
        if (t.getText().equals("")) {
            showDialogErrorMessage("Введите номер заказа");
        } else {
             HttpPost request = new HttpPost("http://daiyn.com/get_docs.php");
        StringEntity params =new StringEntity("order_number="+t.getText());
        request.addHeader("content-type", "application/x-www-form-urlencoded");
        request.setEntity(params);
        HttpResponse response = httpClient.execute(request);

        // handle response here...      
        
        String links = org.apache.http.util.EntityUtils.toString(response.getEntity());
        System.out.println(links);
        if (links.equals("404")) {
            showDialogErrorMessage("Такого заказа не существует");
        } else {
        JSONObject json = new JSONObject(links);
        pagesNumber = json.getInt("total_pages");              
        System.out.println(pagesNumber);
        try (BufferedReader br = new BufferedReader(new FileReader(path1+"page_counter.txt"))) {

            String sCurrentLine;

            while ((sCurrentLine = br.readLine()) != null) {
                System.out.println(sCurrentLine);
                list_count = Integer.parseInt(sCurrentLine);                
            }

	} catch (IOException e) {
            try(PrintWriter out = new PrintWriter("log.txt" )){
                           out.println("Error: " + e.toString());
                    }   
            e.printStackTrace();
	}                    
        
        if (pagesNumber > list_count) {
            showDialogErrorMessage("Недостаточно листов для распечатки");
        } else {
        
        JSONArray jsonarray = json.getJSONArray("docs");
        
        System.out.println(links);        
        links = links.replaceAll("\\[", "").replaceAll("\\]", "");
        links = links.replaceAll("\"", "");  
        String[] linksArr = links.split(",");        
//        for (int i = 1; i < linksArr.length; i++) {
//            linksArr[i] = "http://daiyn.com/uploads/"+t.getText()+"/"+linksArr[i]+".pdf";
//            System.out.println(linksArr[i]);            
//        }
        String[] docsarray = new String[jsonarray.length()];
        int[] copiesNumber = new int[jsonarray.length()];
        
        for (int i = 0; i < jsonarray.length(); i++) {            
            docsarray[i] = "http://daiyn.com/uploads/"+t.getText()+"/"+(String) jsonarray.getJSONObject(i).getString("hash")+".pdf";
            copiesNumber[i] = jsonarray.getJSONObject(i).getInt("list_copies");
            System.out.println(docsarray[i] + " " + copiesNumber[i]);
        }
        
        boolean isPrinter = false;
        
         try (BufferedReader br = new BufferedReader(new FileReader(path1+"printer_on.txt"))) {

            String sCurrentLine;

            while ((sCurrentLine = br.readLine()) != null) {
                System.out.println(sCurrentLine);
                if (sCurrentLine.equals("0")) {
                    isPrinter = false;
                    break;
                } else {
                    isPrinter = true;
                    break;
                }
            }

	} catch (IOException e) {
            try(PrintWriter out = new PrintWriter("log.txt" )){
                           out.println("Error: " + e.toString());
                    }   
            e.printStackTrace();
	}   
        
        if (isPrinter) {
            printFunction(docsarray, copiesNumber);
        } else {
            showDialogErrorMessage("Принтер терминала не работает");
        }
        org.apache.http.util.EntityUtils.consume(response.getEntity());
        t.setText("");
        }
        }
        }
    } catch (Exception ex) {
        // handle exception here
        showDialogErrorMessage("Терминал временно не работает");
        try(PrintWriter out = new PrintWriter("log.txt" )){
                           out.println(list_count+ " ");
                    } catch (FileNotFoundException ex1) {   
                Logger.getLogger(PrintOrder.class.getName()).log(Level.SEVERE, null, ex1);
            }   
    } finally {
        httpClient.getConnectionManager().shutdown();
    }
    }

    
    
    public void printFunction(String[] links, int[] copiesNumber) throws MalformedURLException, IOException, PrinterException {
        
        ArrayList<URL> url = new ArrayList<URL>();
        
        for (int i = 0; i < links.length; i++) {
            url.add(new URL(links[i]));
        }
        
        File f = new File("file.pdf");
        for (int i = 0; i < url.size(); i++) {
            for (int j = 0; j < copiesNumber[i]; j++) {
                FileUtils.copyURLToFile(url.get(i), f);
                PDDocument doc;
                doc = PDDocument.load(f);
                PDFPageable pageable = new PDFPageable(doc);
                System.out.println("Pages number: "+pageable.getNumberOfPages());
                PrinterJob job = PrinterJob.getPrinterJob();
                job.setPageable(new PDFPageable(doc));
                job.print();
            }
      }  
        try(PrintWriter out = new PrintWriter(path1+"page_counter.txt" )){
                           out.println(list_count-pagesNumber);
                    }   
    }

    private void showDialogErrorMessage(String message) {
        JLabel label = new JLabel(message);
            final JOptionPane optionPane = new JOptionPane(label, JOptionPane.INFORMATION_MESSAGE, JOptionPane.DEFAULT_OPTION, null, new Object[]{}, null);
            label.setFont(t.getFont().deriveFont(40));
            optionPane.setBackground(Color.WHITE);            
            final JDialog dialog = new JDialog();
            dialog.setUndecorated(true);
            dialog.setSize(600, 300);
            dialog.setLocationRelativeTo(null);
            dialog.setTitle("Message");
            dialog.setModal(true);            
            dialog.setContentPane(optionPane);
            dialog.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
            dialog.pack();

            //create timer to dispose of dialog after 5 seconds
            Timer timer = new Timer(3000, new AbstractAction() {
                @Override
                public void actionPerformed(ActionEvent ae) {
                    dialog.dispose();
                    t.setText("");
                }
            });
            timer.setRepeats(false);//the timer should only go off once

            //start timer to close JDialog as dialog modal we must start the timer before its visible
            timer.start();

            dialog.setVisible(true);
    }

    private boolean checkInternetConnection() throws InterruptedException, IOException {
        Process p1 = java.lang.Runtime.getRuntime().exec("ping -n 1 www.google.com");
        int returnVal = p1.waitFor();
        boolean reachable = (returnVal==0);
        return reachable;
    }
    
}